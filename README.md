# How to deploy slides to hexacab

    export DOCKERHUB_USER='...'
    docker build -t ${DOCKERHUB_USER}/kubernetes-presentations .
    docker login --username ${DOCKERHUB_USER}
    docker push ${DOCKERHUB_USER}/kubernetes-presentations

    kubectl create namespace presentation
    kubectl -n presentation run presentation --image=varacanero/kubernetes-presentations --port=80
    kubectl -n presentation expose deployment presentation --type=LoadBalancer

    curl presentation.apps.hexacab.org:31889/kubernetes-hello-world.html

    kc -n presentation create -f ./ingress.yml

    curl http://presentation.apps.hexacab.org/kubernetes.html
    curl http://presentation.apps.hexacab.org/kubernetes-hello-world.html

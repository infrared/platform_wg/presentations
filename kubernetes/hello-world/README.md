# Hello world examples for docker or kubernetes

- `./busybox-hello-world`: Focussed on smallest possible docker image (~2MB), based on javascript
- `./nginx-hello-world`: Simple approach of using an index.html file together with [nginx:alpine](https://hub.docker.com/r/library/nginx/tags/alpine/),
  resulting image is ~18MB

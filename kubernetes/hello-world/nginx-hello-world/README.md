# Build

    docker build -t nginx-hello-world .

# Run

    docker run --name nginx-hello-world -p 8080:80 nginx-hello-world

# Greet the world

    curl http://localhost:8080

# Kubernetes Hello Horld

## Create a minimal docker webserver image

    git clone https://github.com/crccheck/docker-hello-world.git
    cd docker-hello-world
    docker build -t busybox-hello-world:v1 .

Check the size - less than 2MB !

    docker images |grep hello-world

Run it locally with docker

    docker run -d --name busybox-hello-world -p 8000:8000 busybox-hello-world:v1
    curl localhost:8000

Stop the container

    docker kill busybox-hello-world

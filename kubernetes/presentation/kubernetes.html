<!DOCTYPE html>
<html>
  <head>
    <title>Title</title>
    <meta charset="utf-8">
    <style>
      @import url(https://fonts.googleapis.com/css?family=Yanone+Kaffeesatz);
      @import url(https://fonts.googleapis.com/css?family=Droid+Serif:400,700,400italic);
      @import url(https://fonts.googleapis.com/css?family=Ubuntu+Mono:400,700,400italic);

      body { font-family: 'Droid Serif'; }
      h1, h2, h3 {
        font-family: 'Yanone Kaffeesatz';
        font-weight: normal;
      }
      img {
            max-width:100%;
      }
      .remark-code, .remark-inline-code { font-family: 'Ubuntu Mono'; }
      .remark-slide-content {background-size: contain}
    </style>
  </head>
  <body>
    <textarea id="source">

![k8s](files/images/kubernetes/k8s.png)
## Pods, Helm and Tiller

---

# Container orchestration

Managing lifecycles of containers

- Provisioning and deployment of containers
- Scaling up or removing containers to spread application load
- Migrating containers from one host to another in case of resources shortage (or a node dies)
- Allocation of resources between containers
- External exposure of services with the outside world
- Health monitoring of containers and hosts
- Configuration of an application

---
# Advantages

- Application isolation
- Atomic entities, good for microservices
- Allows developers to easily interact with infrastructure
- Minimize sysadmin bus factor by using standard, well documented architecture

  > "For the longest time, deploying an application into production was as much ritual as it was science"

- Apps can developed with docker locally and later pushed to kubernetes

---
# Container orchestration landscape

## Cloud services

- Amazon ECS
- Azure Container Service
- Google Container Engine (using Kubernetes)

## FOSS tools

- Kubernetes (formerly borg)
- Openshift (build on kubernetes)
- Docker Swarm
- CoreOS Fleet
- Mesosphere Marathon
- Cattle (from rancher)
- Nomad (Hashicorp)
- Titus (from Netflix)
- Float

---
# Kubernetes

- Open source
- Initially developed by google, now by the Cloud native compute foundation (CNCF)
- Manages app health, replication, load balancing, and hardware resource allocation

---

# Cluster

![cluster](files/images/kubernetes/module_01_cluster.svg)

- From a single node setup (master and node on same VM) to datacenter setups
- [Minikube](https://kubernetes.io/docs/setup/minikube/) for local testing setups

---

# Highlevel Overview

![highlevel](./files/images/kubernetes/kia-cluster-overview.png)

---
# Master

## API Server

- RESTful API
- Authorization and authentication
- `kubectl` as CLI to interact with the master API

## Controller Manager

- Performs cluster-level operations
  - Heartbeat check of nodes
  - Handling node failures
  - Replicating components

---
# Master

## Scheduler

- Manages container deployment to nodes, depending on:
  - CPU
  - Memory
  - How many containers are running?

Deployment requests don't need to consider particular nodes.

---
# Master

## Etcd

- Configuration store for cluster config
- Can be distributed across all hosts

---
# Node

## Kubelet

- Supervisor process
- Manages containers
- Talks to master API

## Container runtime

- Docker
- rkt
- … and possibly more

---
# Node

## Kubernetes Service Proxy (kube-proxy)

Load-balances network traffic between application components

---
# Pod

* Logical entity of containers and volumes with shared resources (Network, storage)

![pod](files/images/kubernetes/module_03_nodes.svg)

---

# Secret

Allow pods to access secrets without shipping them with the container image:

```
kubectl create secret -n testing \
  generic gitlab-token \
  --from-literal=GITLAB_TOKEN="$GITLAB_TOKEN"
```

---

# Configmap

To store application configuration that doesn't need to be protected as secrets.

---

# Volume

- Support for different storage providers
- I.e.
  - hostPath
  - nfs
  - glusterfs
  - gitRepo
  … and many more
- User need to specify a dedicated storage provider

---

# Persistant volume

- Storage provider abstraction layer
- User uses `PersistentVolumeClaim` to request a volume
- Support for:
  - HostPath (only in single-node cluster)
  - NFS
  - iSCSI
  - GlusterFS
  - RBD (Ceph Block Device)
  - GCEPersistentDisk
  - AWSElasticBlockStore
  …

---

# Service

- Abstraction layer for routing traffic
- I.e. exposing a pod's port to become available at a cluster port
- `kube.example.org:3230` -> `12.34.56.78:80`

---

# Ingress

- Allow inbound connection to access a cluster service
- `Nginx`, `haproxy` or others
- Run as additional container
- `dashboard.example.org:80` -> `nginx ingress pod` -> `kube.example.org:3230`

---

# Deployment

![deploy](files/images/kubernetes/module_02_first_app.svg)

---

# Kubernetes Dashboard

![dashboard](files/images/kubernetes/dashboard.png)

---
# Kubectl

![kia-kubectl2](./files/images/kubernetes/kia-kubectl2.png)

---
# Helm

![helm](files/images/kubernetes/helm.png)

* Package manager for kubernetes
* `Tiller`: Server component

---

# Helm usage

![helm install](files/images/kubernetes/helm-install.png)

---

# Kubeapps

![kubeapps](files/images/kubernetes/kubeapps.png)

- Chart repository on https://hub.kubeapps.com/

---

# State of hexacab.org

- 1 Master, 2 Nodes as virtual machines on the same host
- No need for VPN/stunnel between the VMs

## Todo

- Persistant storage
- User management
- Gitlab integration
…

See https://0xacab.org/infrastructure/platform/boards

---

# Security considerations

- Containers: Just `Linux namespaces` + `Linux control groups`
- [Linux namespaces](https://medium.com/@teddyking/linux-namespaces-850489d3ccf)
- ([cgroups](https://en.wikipedia.org/wiki/Cgroups)) Kernel feature for limitation and isolation of resource usage for processes
- Even possible to allow container to fully access host system.
  - Scenario: OS that don't allow to install *anything* on the host
  - How do you do backup / monitoring ?
- With great power comes great responsibilty

---

# Security considerations

- Worthwhile reads:
  - [Containers vs. Zones vs. Jails vs. VMs](https://blog.jessfraz.com/post/containers-zones-jails-vms/)


> "VMs, Jails, and Zones are if you bought the legos already put together AND glued.
> So it’s basically the Death Star and you don’t have to do any work - you get it pre-assembled out of the box. You can’t even take it apart.

> Containers come with just the pieces so while the box says to build the Death Star, you are not tied to that.
> You can build two boats connected by a flipping ocean and no one is going to stop you.

> This kind of flexibility allows for super awesome things but of course comes at a price."

---

# Threats

1. External attacks aiming to compromise Kubernetes controls
1. Compromised containers/nodes
1. Compromised credentials
1. Misuse of legitimate privileges

further reading: `Kubernetes Deployment & Security Patterns`

---

# How to maintain docker images

- Scan images for vulnerabilities on a regular base
  - include underlying images
- Either build images yourself or use a trusted third party registry
- Use own (private) registry
- Don't use arbitrary docker images from docker hub!

---

# Dockerhub vulnerability scan

![dockerhub-vulnerabilities](./files/images/kubernetes/dockerhub-vulnerabilities.png)

---

# Gitlab autodevops scanning

Using [clair](https://github.com/coreos/clair) and [clair-scanner](https://github.com/arminc/clair-scanner).
See [gitlab container scanning](https://docs.gitlab.com/ee/ci/examples/container_scanning.html)_for details.

![gitlab-autodevops-jobs](./files/images/kubernetes/gitlab-autodevops-jobs.png)

---
# Are you lost ?

![cnl](files/images/kubernetes/cln-landscape.jpeg)

---
# Deployment overview

![kia-kubectl1](./files/images/kubernetes/kia-kubectl1.png)

---
# Deployment overview

![](./files/images/kubernetes/kia-deployment.png)

    </textarea>
    <script src="https://remarkjs.com/downloads/remark-latest.min.js">
    </script>
    <script>
      var slideshow = remark.create();
    </script>
  </body>
</html>

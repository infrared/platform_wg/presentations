# Build:
#
#    docker build -t ${USER}/kubernetes-presentations .
#
# Run:
#
#     docker run --rm --name kubernetes-presentations -p 8080:80 ${USER}/kubernetes-presentations
#
# Browse:
# i.e.
#
#    http://localhost:8080/kubernetes.html
#    http://localhost:8080/kubernetes-hello-world.html

FROM nginx:alpine
COPY --chown=nginx files /usr/share/nginx/html/files/
COPY --chown=nginx infracon /usr/share/nginx/html/infracon/
COPY --chown=nginx kubernetes /usr/share/nginx/html/kubernetes/

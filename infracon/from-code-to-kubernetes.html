<!DOCTYPE html>
<html>
  <head>
    <title>From lines of code to Kubernetes</title>
    <meta charset="utf-8">
    <style>
      @import url(https://fonts.googleapis.com/css?family=Yanone+Kaffeesatz);
      @import url(https://fonts.googleapis.com/css?family=Droid+Serif:400,700,400italic);
      @import url(https://fonts.googleapis.com/css?family=Ubuntu+Mono:400,700,400italic);

      body { font-family: 'Droid Serif'; }
      h1, h2, h3 {
        font-family: 'Yanone Kaffeesatz';
        font-weight: normal;
      }
      img {
            max-width:100%;
      }
      .remark-code, .remark-inline-code { font-family: 'Ubuntu Mono'; }
      .remark-slide-content {background-size: contain}
      .footnote { position: absolute; bottom: 3em; }
      blockquote { border-left: 0.3em solid rgba(0,0,0,0.5); padding: 0 15px; font-style: italic; }
    </style>
  </head>
  <body>
    <textarea id="source">
class: center
# From lines of code to Kubernetes
## Containerising and deploying your applications

<img src="files/images/container-fun/140222145646-05-svendborg-horizontal-large-gallery.jpg" width="600">

---
# Overview

* Part 1: The twelve-factor app
* Part 2: Deploy a simple application to kubernetes

---
# Part 1: The twelve-factor app

Methology published by Heroku¹ "for building
software-as-a-service apps that:"

* Use declarative formats for setup automation, to minimize time and cost for new developers joining the project;
* Have a clean contract with the underlying operating system, offering maximum portability between execution environments;
* Are suitable for deployment on modern cloud platforms, obviating the need for servers and systems administration;
* Minimize divergence between development and production, enabling continuous deployment for maximum agility;
* And can scale up without significant changes to tooling, architecture, or development practices.

.footnote[.red.bold[¹] https://12factor.net]
---
# I. Codebase

## One codebase tracked in revision control, many deploys.

* If there are multiple codebases, it's not an app -- it's a distributed system. Each component in a distributed system is an app, and each can individually comply with twelve-factor.
* Multiple apps sharing the same code is a violation of twelve-factor. The solution here is to factor shared code into libraries which can be included through the dependency manager.

---
# II. Dependencies

## Explicitly declare and isolate dependencies

Never rely on implicit existence of system-wide packages.

* Dependency declaration manifest (Gemfile, requirements.txt, etc.)
* Dependency isolation tool (bunder, virtualenv, etc.)

No matter what the toolchain, dependency declaration and isolation must always
be used together.
If the app needs to shell out to a system tool, that tool should be vendored
into the app.

---
# III. Config

## Store config in the environment

Strict separation of config from code - can you open source your app from one
day to the other without revealing secrets ?

* Store config in environment variables.
* Don't group config into "environments" named after specific deploys (i.e.
  *development*, *test*, and *production*).
* Allow granular control of config variables.

---
# IV. Backing services

## Treat backing services as attached resources

* No distinction between local services (datastores, messaging/queueing systems, etc.)
  and third party services (S3, twitter API etc.).
* Make it easy to swap out local against third party service, and to de-/attach
  a resource (i.e. a MySQL db).


---
# V. Build, release, run

## Strictly separate build and run stages

* *Build*: Converts a code repo into an executable bundle
  (i.e. building the public docker image)
* *Release*: Takes build and add deploy's current config
  (i.e. kubernetes deployment config, helmfile)
* *Run*: Runs the app in the execution environment
  (i.e. kubernetes ReplicaSet controller)

---
# VI. Processes

## Execute the app as one or more stateless processes

* Twelve-factor processes are stateless and share-nothing.
* Persitant data must be stored in a stateful backing service, typically a db.
* Use RAM or filesystem only as a brief, single-transaction cache.
* Use datastore like Redis/Memcached to cache user sessions data in memory.


---
# VII. Port binding

## Export services via port binding

* The twelve-factor app is completely self-contained.
* Don't rely on any external service (like a webserver) to create a web-facing service.
* Export HTTP (or other protocols) as a service by binding to a port.

---
# VIII. Concurrency

## Scale out via the process model

<img src="files/images/12factors-process-types.png" width="300">

* Break down your app into different components
* Different processes/containers per app component


---
# IX. Disposability

## Maximize robustness with fast startup and graceful shutdown

* Minimize startup time: easier scaling and rescheduling
* Graceful shutdown on SIGTERM, i.e.
  * Stop accepting new requests
  * Finish current requests
  * Return unfinished worker jobs to queue
* Robust against sudden death


---
# X. Dev/prod parity

## Keep development, staging, and production as similar as possible

* Continuous deployment by keeping the gap between development and production small
* I.e. avoid docker for staging, and debian packages for production
* Same backend for development and production: Don't use SQLite for dev
  and postgres for prod


---
# XI. Logs

## Treat logs as event streams

* The app shouldn't deal with routing or storage of its output stream itself.
* Stream events to `stdout`.
* Execution environment is responsible for log management/storage.
  * Allows log aggregation of multiple apps, long-time storage etc.

---
# XII. Admin processes

## Run admin/management tasks as one-off processes

* Admin tasks i.e.
  * DB migrations
  * Running a (REPL) console to interact with the app
  * One-time maintenance/repair tasks

* Scripts must ship with application code
* Same dependency isolation as in production, i.e. `bundle exec`
---
# Part 2: Deploy a simple app to kubernetes

Environemt: Your kubernetes cluster

Depending on your access to a public docker registry, you can also build
the app on your laptop.


---
# Docker image

Let's start with a simple docker image:

```bash
$ git clone https://0xacab.org/infrared/platform_wg/presentations.git
$ cd presentations/kubernetes/hello-world/nginx-hello-world
$ ls

Dockerfile  index.html  README.md
```

```bash
$ cat index.html
Hello World !
```

```bash
$ cat Dockerfile
FROM nginx:alpine
COPY --chown=nginx index.html /usr/share/nginx/html
```

---
# Build the image

```bash
$ docker build -t nginx-hello-world .
…
```

Show the image in your local docker registry:

```bash
$ docker images | grep nginx-hello-world
nginx-hello-world   latest   d485d90c479a  1 minute ago  17.7MB
```

---
# Run a container locally

```bash
$ docker run --rm --name nginx-hello-world -p 8080:80 nginx-hello-world
```

# Greet the world

```bash
$ curl http://localhost:8080
Hello World !
```

---
# Publish the image (in case you built on your laptop)

Options:

- [hub.docker.com](https://hub.docker.com/)
- 0xacab.org

```bash
$ docker login 0xacab.org:4567

$ docker tag nginx-hello-world 0xacab.org:4567/infrared/platform_wg/presentations/test

$ docker push 0xacab.org:4567/infrared/platform_wg/presentations/test
```

See https://0xacab.org/infrared/platform_wg/presentations/container_registry for uploaded image.

---
# Install kubectl

Follow the [kubectl](https://kubernetes.io/docs/tasks/tools/install-kubectl/) installation instructions.

---
# Configure kubectl

`kubectl` needs to access your cluster API.

```bash
scp root@CLUSTER:/etc/kubernetes/.tmp/kubecfg-kube-admin.yaml ~/.kube/config
```

Test if it works:

```bash
kubectl cluster-info
```

---
# Create namespace

```bash
$ kubectl create namespace demo
$ kubectl get namespace
```

---
# Create a deployment

```bash
$ kubectl -n demo run nginx-hello-world --port=80 \
    --image=0xacab.org:4567/infrared/platform_wg/presentations/test
```

See what resources are created:

```bash
$ kubectl -n demo get all
```

→ Deployment
  → Replicaset
    → Pod

---
# Deployment

High-level resource for running and updating applications.

- Container Image, Pull strategy
- Network Ports
- How many replicas
- Update strategy
- Creates a `ReplicaSet`

---
# ReplicaSet

- ReplicaSet-Controller starts and supervises `Pods`

---
# Pod

* Logical entity of containers and volumes with shared resources (Network, storage)

![pod](files/images/kubernetes/module_03_nodes.svg)

---
# Show pod config and events

```bash
kubectl -n demo describe pod/nginx-hello-world-6cb6f8c584-4564f
```

---

# Expose port / create Service

Create a service to expose the pod port (which is reachable internally only) to be reachable from outside:

```bash
$ kubectl -n demo expose deployment nginx-hello-world --type=LoadBalancer
```

Lookup the exposed port from the created service:

```bash
$ kubectl -n demo get all

…
service/nginx-hello-world   LoadBalancer   10.43.159.209   <pending>     80:32273/TCP   4s
…
```

---

# First Hello world !

```bash
$ curl varac-oas.openappstack.net:32273
Hello World !
```

Disadvantages of `LoadBalancer service`:

- Randomly assigned port ≠ 80
- No TLS !

---
# Ingress controller

- Acts as a reverse proxy or load balancer
- Single, central service for the cluster
- (Sub-)Domain and path as denominator
- Takes care of TLS cert retrieval, i.e. using Letsencrypt

---
# Ingress controller

<img src="files/images/kubernetes/ingress.svg" height="500" width="900">


---
# Traefik

![:img Traefik, 30%](files/images/traefik.logo.svg)

OpenAppStack pre-installs [traefik](https://traefik.io/) as ingress controller.

---
# Create ingress resource

`infracon/ingress.yml`:

```yaml
apiVersion: extensions/v1beta1
kind: Ingress
metadata:
  name: demo
  annotations:
    kubernetes.io/ingress.class: traefik
*    kubernetes.io/tls-acme: "true"
spec:
  rules:
*  - host: demo.varac-oas.openappstack.net
    http:
      paths:
      - path: /
        backend:
          serviceName: nginx-hello-world
          servicePort: 80
```

```bash
$ kubectl -n demo create -f infracon/ingress.yml
```

---
# Show ingress resource

```bash
$ kubectl -n demo get all
```

→ [`kubectl get all` *doesn't* get all](https://github.com/kubernetes/kubectl/issues/151),
use the [kubectl ketall plugin](https://github.com/corneliusweig/ketall) instead.

```bash
$ kubectl -n demo describe ingress
```

---
# Hello World with TLS

```bash
$ curl https://demo.varac-oas.openappstack.net/
Hello World !
```

Traefik dashboard: https://traefik.varac-oas.openappstack.net/


---
# Get events

```bash
kubectl -n demo get pods
kubectl -n demo
```

---
# Get logs

```bash
kubectl -n demo get pods
kubectl -n demo logs nginx-hello-world-6cb6f8c584-xrhwh
```

---
# Thanks !

- info@openappstack.net
- https://openappstack.net/
- IRC: #oas @ freenode.net

    </textarea>
    <script src="https://remarkjs.com/downloads/remark-latest.min.js">
    </script>
    <script>
     remark.macros.img = function (altText, width) {
       var url = this;
       return '<img alt="' + altText + '" src="' + url + '" style="width: ' + width + '" />';
      };
      var slideshow = remark.create({
        highlightLines: true
      });
    </script>
  </body>
</html>
